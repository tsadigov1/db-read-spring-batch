package az.abb.dbreadspringbatch.entity;

import java.util.UUID;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "message")
public class Message {
    @Id
    @Column(name = "id")
    @GeneratedValue
    private UUID id;

    @PrePersist
    private void prePersist() {
        id = UUID.randomUUID();
    }

    @Column(name = "message")
    private String message;
    @Column(name = "phone")
    private String phone;
    @Column(name = "app_user_id")
    private String appUserId;
    @Column(name = "status")
    private String status;
    @Column(name = "bulk_process_id")
    private Long bulkProcessId;
    @Column(name = "json_content")
    private String jsonContent;
    @ManyToOne
    @JoinColumn(name = "bulk_process_id", insertable = false, updatable = false)
    private BulkProcess bulkProcess;

//    @ManyToOne(cascade = CascadeType.ALL)
//    @JoinColumn(name = "card_uid", referencedColumnName = "uid")
//    private Card card;

}
