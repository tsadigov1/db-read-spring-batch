package az.abb.dbreadspringbatch.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity
@Data
public class Customer {
    @Id
    long id;

    @Column
    String name;

    @Column
    BigDecimal credit;
}
