package az.abb.dbreadspringbatch.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "bulk")
public class BulkProcess {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;
    @Column(name = "bulk_name")
    private String bulkName;
    @Column(name = "cycle")
    private String cycle;
    @Column(name = "cycle_period")
    private String cyclePeriod;
    @Column(name = "status")
    //    ENUM
    private String status;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bulkProcess")
    private List<Message> messages;

}
