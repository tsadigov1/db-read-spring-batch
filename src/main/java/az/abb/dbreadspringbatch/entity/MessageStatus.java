package az.abb.dbreadspringbatch.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "message_status")
public class MessageStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "bulk_process_id", insertable = false, updatable = false)
    private BulkProcess bulkProcess;

    @Column(name = "control_id")
    private String controlId;
}
