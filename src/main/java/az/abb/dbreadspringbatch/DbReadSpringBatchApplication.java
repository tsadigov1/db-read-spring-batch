package az.abb.dbreadspringbatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbReadSpringBatchApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbReadSpringBatchApplication.class, args);
	}

}
