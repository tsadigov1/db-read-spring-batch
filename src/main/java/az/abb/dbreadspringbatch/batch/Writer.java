package az.abb.dbreadspringbatch.batch;

import az.abb.dbreadspringbatch.entity.Customer;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class Writer implements ItemWriter<Customer> {
    @Override
    public void write(List<? extends Customer> items) throws Exception {
        System.out.println("**** Start of writer");
        for (Customer customer: items) {
            System.out.println(customer.toString());
        }
        System.out.println("**** End of writer\n");
    }
}