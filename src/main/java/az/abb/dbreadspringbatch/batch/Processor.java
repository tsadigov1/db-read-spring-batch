package az.abb.dbreadspringbatch.batch;

import az.abb.dbreadspringbatch.entity.Customer;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class Processor implements ItemProcessor<Customer, Customer> {

    @Override
    public Customer process(Customer customer) throws Exception {
        System.out.println("Processor: " + customer.toString());
        return customer;
    }
}
